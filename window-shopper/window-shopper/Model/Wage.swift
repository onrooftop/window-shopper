//
//  Wage.swift
//  window-shopper
//
//  Created by Panupong Kukutapan on 10/31/2560 BE.
//  Copyright © 2560 Panupong Kukutapan. All rights reserved.
//

import Foundation


class Wage {
    class func getHour(forWage wage: Double, andPrice price: Double) -> Int {
        return Int(ceil(price / wage))
    }
}
